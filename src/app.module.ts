import { Module } from '@nestjs/common';
import {
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { v4 } from 'uuid';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@WebSocketGateway(4001, {
  allowEIO3: true,
})
class AppGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  // @WebSocketServer() server;
  afterInit(server: any) {
    console.log('Initialize');
    setInterval(() => {
      server.emit('feed', v4());
    }, 1000);
  }

  handleConnection(client: any, ...args: any[]) {
    console.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: any) {
    console.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('hello')
  hello(@MessageBody() data: any) {
    console.log(data);
    return { hi: 'there' };
  }
}

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppGateway, AppService],
})
export class AppModule {}
