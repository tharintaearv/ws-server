FROM node:latest AS builder

COPY package.json ./
RUN yarn

COPY . ./
RUN yarn build

FROM node:alpine
WORKDIR /src/bin/app
COPY --from=builder /node_modules ./node_modules
COPY --from=builder /dist ./dist
COPY --from=builder package.json .

EXPOSE 4000
EXPOSE 4001
CMD ["yarn", "start:prod"]